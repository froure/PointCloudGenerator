<h1>Point Cloud Generator</h1>
<p>This program generates different types of synthetic point clouds according to the user's preferences. The resulting point clouds are saved in a *.ply file format.</p>

<h2>Updates</h2>
<p>
    <h3>15/02/16</h3>
    <p>
        Available point cloud types:<br>
        - Random <br>
        - Fast Spiral (linear) <br>
        - Medium Spiral (square root) <br>
        - Low Spiral (logarithmic) <br>
    </p>
</p>